<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;
Route::rule('api/:action/[:method]', 'api/apiPage', 'GET');
Route::rule('api/:action/[:method]', 'api/apiInit', 'POST');
Route::rule('apidocs', 'apidocs/apiList');
Route::rule('login', 'index/login');
Route::rule('download', 'index/download');
Route::rule('logout', 'index/logout');
Route::rule('share/:action', 'index/share');
//Route::rule('api/:id', 'api/apiList','*',array(),['id'=>'\d+']);
return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    
   // 'api/:hash'=>['api/apiPage',['method'=>'get']],
   // 'api/:hash'=>['api/apiInit',['method'=>'post']],
    //'api/:action/:method'=>['api/apiInit',['method'=>'post']],
    //'api/:action/:method'=>['api/apiPage',['method'=>'get']],
];
