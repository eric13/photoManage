<?php
/**
 * 系统常量配置。系统默认加载
 * 
 *  * ============================================================================
 * 版权所有 2017北京素玄科技，并保留所有权利。
 * 
 * 网站地址: http://www.suxuantech.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经允许的情况下，您不能对本系统代码做任何修改 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如有修改需求，请联系素玄科技有限公司：contact@suxuantech.cn
 * ============================================================================
 * $Author: songdemei<songdemei@suxuantech.cn> 2017-10-17 $
 */

if(!defined('ERR_LOGIN_EXPIRED')){
    define('ERR_LOGIN_EXPIRED', -1);
}
if(!defined('ERR_LOGIN_UNLOGIN')){
    define('ERR_LOGIN_UNLOGIN', -2);
}
if(!defined('ERR_ACCESSTOKEN_EMPTY')){
    define('ERR_ACCESSTOKEN_EMPTY', -3);
}
if(!defined('ERR_ACCESSTOKEN_ERROR')){
    define('ERR_ACCESSTOKEN_ERROR', -4);
}

