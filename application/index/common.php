<?php
/**
 * 基础类的基础函数库
 * 
 * ============================================================================
 * 版权所有 2017北京素玄科技，并保留所有权利。
 * 
 * 网站地址: http://www.suxuantech.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经允许的情况下，您不能对本系统代码做任何修改 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如有修改需求，请联系素玄科技有限公司：contact@suxuantech.cn
 * ============================================================================
 * $Author: songdemei<songdemei@suxuantech.cn> 2017-10-18 $
 */

/**
* 获取本系统内的所有模块信息。
* 
* @return Array 所有模块列表数组
* @author songdemei<songdemei@suxuantech.cn> 2017-10-18
*/
function getModules() {
    $list = scandir(APP_PATH);
    $moduleList = array();
    foreach ($list as $dir){
        //echo $dir;
        if(substr($dir,0,1) == '.'){
//                过虑隐藏文件或.开头的文件
            continue;
        }
        if(in_array($dir, array('extra'))){
            //过虑默认不需要展示的模块名称
            continue;
        }
        if(preg_match('/^[a-zA-Z\s]+$/', $dir)){
            $test = new \ReflectionClass('app\\'.$dir.'\controller\index');
            if(!$test->hasMethod('getModuleInfo')){
                continue;
            }
            $dir = $dir.'/index';
            $controller = controller($dir);
            $action = action($dir.'/getModuleInfo');
            if($controller && $action){
                $res = @$controller->getModuleInfo();
                $moduleInfo = json_decode($res,true);
                $moduleInfo['url'] = $dir;
                $moduleList[] = $moduleInfo;
            }
        }
        unset($dir);
    }
    return $moduleList;
}