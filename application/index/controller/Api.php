<?php
/**
 * 项目API的公共类，所有模块内的API要引用此模块，进行业务公共逻辑处理。
 * 
 *  * ============================================================================
 * 版权所有 2017北京素玄科技，并保留所有权利。
 * 
 * 网站地址: http://www.suxuantech.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经允许的情况下，您不能对本系统代码做任何修改 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如有修改需求，请联系素玄科技有限公司：contact@suxuantech.cn
 * ============================================================================
 * $Author: songdemei<songdemei@suxuantech.cn> 2017-10-17 $
 */
namespace app\index\controller;

class Api extends Base {
    
    function _initialize() {
        //如果是API接口，需要在此处定义is_api为true
        $this->apiStartTime = microtime(true);
        define('IS_API',TRUE);
        if(!defined('IS_POST')){
            define('IS_POST', \think\Request::instance()->isPost());
        }
        parent::_initialize();
    }
    /**
     * 
     * 返回本系统的所有已安装模块信息
     * 
     * @return json
     * @author songdemei<songdemei@suxuantech.cn> 2017-10-18
     */
    public function getInstallModules() {
        //$content = httpRequest('http://crm.sx.com/index/index');
        //var_dump($content);
       return $this->apiReturn(getModules());
    }
    
    public function getModuleInfo() {
        return array();
    }
    
    /**
     * 
     * 应用接口入口
     * 
     */
    public function apiInit() {
        
        $urlArr = explode('/', $this->apiUrl);
        $module = 'index';
        if(count($urlArr) == 3){
            $module = $urlArr[0];
            $action = $urlArr[1];
            $method = $urlArr[2];
            
        }else{
            $action = $urlArr[0];
            $method = $urlArr[1];
        }
        //list($action, $method) = $urlArr;
        $instance = new \ReflectionClass('app\\'.$module.'\\api\\'.$action);
        if (!$instance->hasMethod($method)) {
            $this->apiError('没有发现这个接口.', 404);
            //Response::error(ReturnCode::EXCEPTION, '服务器端配置异常');
        }
        $objMethod = $instance->getMethod($method);
        $handle = $instance->newInstance();
        $data = $objMethod->invokeArgs($handle, array($this->apiParams));
        if(!is_array($data)){
            $this->apiError('无效的接口返回，请联系技术人员，接口地址：'.$this->apiUrl, 404);
        }
        $this->apiEndTime = microtime(true);
        
        if($data['error']>0){
            $this->apiError($data['code'], $data['msg']);
        }else{
            $this->apiReturn($data);
        }
        
    }
    
    
    /**
     * API的展示入口
     * 
     * @return type
     */
    public function apiPage() {
        if($this->apiHash){
            //api详情，含接口调试
            $apiInfo = db('api_list')
                    ->where('hash',  $this->apiHash)
                    ->find();
            $appInfo = db('api_app')
                    ->find($apiInfo['app_id']);
            $fieldList = db('api_fields')->where('hash',  $this->apiHash)->where('type',0)->select();
            
            $this->assign('apiInfo',$apiInfo);
            $this->assign('appInfo',$appInfo);
            $this->assign('fieldList', $fieldList);
            $this->assign('domain',\think\Request::instance()->domain());
            return $this->fetch();
        }
        
        
        
    }

    
            
}