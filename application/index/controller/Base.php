<?php
/**
 * CRM项目模块公共类，所有开发的模块要引用此模块，进行业务公共逻辑处理。
 * 主要处理运行初始化，用户认证，日志，
 * ============================================================================
 * 版权所有 2017北京素玄科技，并保留所有权利。
 * 
 * 网站地址: http://www.suxuantech.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经允许的情况下，您不能对本系统代码做任何修改 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如有修改需求，请联系素玄科技有限公司：contact@suxuantech.cn
 * ============================================================================
 * $Author: songdemei<songdemei@suxuantech.cn> 2017-10-17 $
 */
namespace app\index\controller;
use think\Controller;
class Base  extends Controller{
    /**
     * API的hash值
     * 
     * @var type String
     */
    protected $apiHash = '';
    protected $apiUrl = '';
    protected $appSecret = '';
    protected $apiParams = array();
    protected $apiStartTime = 0;
    protected $apiEndTime = 0;
    function _initialize() {
        
        if(defined('IS_API') && IS_API === true){
            $input = input();
            $this->apiHash = $input['action'];
            if(defined('IS_POST') && IS_POST === true){
                //验证接口加密规范 暂时只支持使用hash调用，后期再考虑可以支持url调用。--songdemei
                //if(!isset($input['method']) || empty($input['method'])){
                    //
                    
                
                    return $this->checkAPI();
                //}else{
                    /*
                    $apiUrl = $input['action'].'/'.$input['method'];
                    $apiList = db('apilist')->find(array('apiUrl'=>$apiUrl));
                    */
                    
                    
                    
                //}


            }



            //ECHO 'base init =-----<<<<<<br/>';
        }
        
    }
    private function checkAPI() {
        $apiInfo = db('apiList')
                ->join('api_app', 'api_app.id = api_list.app_id', 'left')
                ->field('api_list.*,api_app.app_name,api_app.app_id,api_app.app_secret,api_app.app_status,api_app.app_type')
                ->where('api_list.hash',  $this->apiHash)
                ->find();
        if(!$apiInfo){
            $this->apiError('接口不存在', -1001);
            
        }
        if($apiInfo['status'] != 1){
            $this->apiError('接口被禁用', -1002);
        }
        if($apiInfo['app_status'] != 1){
            $this->apiError('应用已禁用', -1003);
        }
        if($apiInfo['accessToken'] == 1){
            $accessToken = request()->header('ACCESS-TOKEN');
            if(!$accessToken){
                $this->apiError('当前接口需要token验证。', ERR_ACCESSTOKEN_EMPTY);
            }
            
            if(!checkAccessToken($apiInfo['app_secret'],$accessToken)){
                $this->apiError('AccssToken check error。', ERR_ACCESSTOKEN_ERROR);
            }
        }
        if($apiInfo['needLogin'] == 1){
            $sessionId = request()->header('SESSION-ID');
            if(!$sessionId){
                $this->apiError('当前接口需要登录才能使用。', ERR_LOGIN_UNLOGIN);
            }
            if(!checkLogin($sessionId)){
                $this->apiError('用户未登录，或登录信息已过期。', ERR_LOGIN_EXPIRED);
            }
            
        }
        
        $this->apiUrl = $apiInfo['apiUrl'];
        $this->appSecret = $apiInfo['app_secret'];
        $this->apiParams = input('post.');
    }
    function actionLog() {
        
        
        
    }
    public function apiReturn ($data) {
        apiLog(array(
            'api_url'=>  $this->apiUrl,
            'hash'=>  $this->apiHash,
            'data'=> json_encode($this->apiParams),
            'api_start_time'=> $this->apiStartTime,//number_format($this->apiStartTime, 4,'.',''),
            'api_end_time'=>  $this->apiEndTime,//number_format($this->apiEndTime,4,'.',''),
            'result_code'=>'200',
        ));
        
        $ret = array('code'=>200,'msg'=>'','data'=>$data);
        exit(json_encode($ret));
        //return json($data,200);
    }
    public function apiError($msg,$code) {
        $ret = array('code'=>$code,'msg'=>$msg,'data'=>array());
        apiLog(array(
            'api_url'=>  $this->apiUrl,
            'hash'=>  $this->apiHash,
            'data'=> json_encode($this->apiParams),
            'api_start_time'=>  $this->apiStartTime,//number_format($this->apiStartTime,4,'.',''),
            'api_end_time'=>  $this->apiEndTime,//number_format($this->apiEndTime,4,'.',''),
            'result_code'=>$code,
        ));
        send_http_status(500);
        exit(json_encode($ret));
    }
    
}