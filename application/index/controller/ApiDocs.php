<?php
/**
 * 项目API的公共类，所有模块内的API要引用此模块，进行业务公共逻辑处理。
 * 
 *  * ============================================================================
 * 版权所有 2017北京素玄科技，并保留所有权利。
 * 
 * 网站地址: http://www.suxuantech.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经允许的情况下，您不能对本系统代码做任何修改 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如有修改需求，请联系素玄科技有限公司：contact@suxuantech.cn
 * ============================================================================
 * $Author: songdemei<songdemei@suxuantech.cn> 2017-10-17 $
 */
namespace app\index\controller;
use think\Controller;
class ApiDocs  extends Controller{

    /**
     * api 列表页，展示所有API列表，不传参数时，列出所有应用，传app_id时，列出此应用下的所有API（含禁用的）
     * @return type
     */
    public function apiList() {
        $input = input();
        if($input['app_id']){
            $apiList = db('api_list')->where('app_id',$input['app_id'])->select();
            $this->assign('apiList',$apiList);
        }else{
            $apiList = db('api_app')->select();
            $this->assign('appList',$apiList);
        }
        
        return $this->fetch();
        
    }
    /**
     * 签名参数，将所有POST参数，按access token规则进行签名，以获得API的ACCESS-TOKEN
     * 只能在调试模式下调用，非debug模式不能调用。
     * 
     */
    public function signParams() {
        if(config('app_debug') === true){
            $params = input('post.');
            $appId = $params['app_id'];
            unset($params['app_id']);
            $appInfo = db('api_app')->where('app_id',$appId)->find();
            $appSecret = $appInfo['app_secret'];
            ksort($params);
            $tokenStr = md5(arrayToParams($params).$appSecret);
            exit(json_encode(array('code'=>200,'msg'=>'','data'=>array('sign'=>$tokenStr))));
        }else{
            send_http_status(500);
            exit(json_encode(array('code'=>500,'msg'=>'AccessDeny!','data'=>array())));
        }
    }
    
}