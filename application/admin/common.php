<?php
/**
 * 系统非常规MD5加密方法
 * @param  string $str 要加密的字符串
 * @param  string $auth_key 要加密的字符串
 * @return string
 * @author jry <598821125@qq.com>
 */
function user_md5($str, $auth_key = ''){
    if(!$auth_key){
        $auth_key = config('AUTH_KEY');
    }
    return '' === $str ? '' : md5(sha1($str) . $auth_key);
}

/**
 * 判断是否是系统管理员
 * @param mixed $uid
 * @return bool
 */
function isAdministrator( $uid = '' ){
    if( empty($uid) ) $uid = session('uid');
    if( is_array(config('USER_ADMINISTRATOR')) ){
        if( is_array( $uid ) ){
            $m = array_intersect( config('USER_ADMINISTRATOR'), $uid );
            if( count($m) ){
                return TRUE;
            }
        }else{
            if( in_array( $uid, config('USER_ADMINISTRATOR') ) ){
                return TRUE;
            }
        }
    }else{
        if( is_array( $uid ) ){
            if( in_array(config('USER_ADMINISTRATOR'),$uid) ){
                return TRUE;
            }
        }else{
            if( $uid == config('USER_ADMINISTRATOR')){
                return TRUE;
            }
        }
    }
    return FALSE;
}

/**
 * 把返回的数据集转换成Tree
 * @param $list
 * @param string $pk
 * @param string $pid
 * @param string $child
 * @param string $root
 * @return array
 */
function listToTree($list, $pk='id', $pid = 'fid', $child = '_child', $root = '0') {
    $tree = array();
    if(is_array($list)) {
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = &$list[$key];
        }
        foreach ($list as $key => $data) {
            $parentId =  $data[$pid];
            if ($root == $parentId) {
                $tree[] = &$list[$key];
            }else{
                if (isset($refer[$parentId])) {
                    $parent = &$refer[$parentId];
                    $parent[$child][] = &$list[$key];
                }
            }
        }
    }
    return $tree;
}

function formatTree($list, $lv = 0, $title = 'name'){
    $formatTree = array();
    foreach($list as $key => $val){
        $title_prefix = '';
        for( $i=0;$i<$lv;$i++ ){
            $title_prefix .= "|---";
        }
        $val['lv'] = $lv;
        $val['namePrefix'] = $lv == 0 ? '' : $title_prefix;
        $val['showName'] = $lv == 0 ? $val[$title] : $title_prefix.$val[$title];
        if(!array_key_exists('_child', $val)){
            array_push($formatTree, $val);
        }else{
            $child = $val['_child'];
            unset($val['_child']);
            array_push($formatTree, $val);
            $middle = formatTree($child, $lv+1, $title); //进行下一层递归
            $formatTree = array_merge($formatTree, $middle);
        }
    }
    return $formatTree;
}

if (!function_exists('array_column')) {
    function array_column($array, $val, $key = null){
        $newArr = array();
        if( is_null($key) ){
            foreach ($array as $index => $item) {
                $newArr[] = $item[$val];
            }
        }else{
            foreach ($array as $index => $item) {
                $newArr[$item[$key]] = $item[$val];
            }
        }
        return $newArr;
    }
}
function delDirAndFile($dirName) {
    if(is_dir($dirName)){
        if ($handle = opendir("$dirName")) {
            while (false !== ( $item = readdir($handle) )) {
                if ($item != "." && $item != "..") {
                    if (is_dir("$dirName".DS."$item")) {
                        $res = delDirAndFile("$dirName".DS."$item");
                        if(!$res){
                            return false;
                        }
                    } else {
                        if (!unlink("$dirName".DS."$item")){
                            return false;
                        }
                    }
                }
            }
            closedir($handle);
            if (rmdir($dirName)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }else{
        if (!unlink($dirName)){
            return false;
        }
    }
    return true;
}
/**
 * 获取当前配置内，上传文件的最大限制
 * 
 * @return type
 */
function getMaxUploadSize(){
    
    $scan['gb'] = 1073741824; //1024 * 1024 * 1024;
    $scan['g']  = 1073741824; //1024 * 1024 * 1024;
    $scan['mb'] = 1048576;
    $scan['m']  = 1048576;
    $scan['kb'] =    1024;
    $scan['k']  =    1024;
    $scan['b']  =       1;
    $upSize = strtolower(ini_get('upload_max_filesize'));
    $upSizeC = str_replace(intval($upSize) . '','',$upSize);
    $upSizeN = $scan[$upSizeC] * intval($upSize);
    $postSize = strtolower(ini_get('post_max_size'));
    $postSizeC = str_replace(intval($postSize) . '','',$postSize);
    $postSizeN = $scan[$postSizeC] * intval($postSize);
    
    $limit = $postSizeN>$upSizeN?$upSize:$postSize;
    
    return $limit;
    //$upSize = $scan[strtolower(ini_get('upload_max_filesize'))] ;
    
//    return ini_get('upload_max_filesize');    
    
}
function S($name,$value="",$options = null) {
    return cache($name, $value, $options);
}
function I($key,$default = null,$filter = '') {
    return input($key,$default,$filter);
}
function C($name,$value=null,$range='') {
    return config($name,$value,$range);
}
function D($param) {
    return db($param);
}
function U($url = '', $vars = '', $suffix = true, $domain = false) {
    return url($url , $vars, $suffix, $domain);
}
function L($string){
    return Lang($string);
}