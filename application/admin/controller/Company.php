<?php



namespace app\admin\controller;


class Company extends Base {
    //put your code here
    
    function Index() {
        
        $list = D('ApiCompany')->select();
        $companyList = $this->buildArrByNewKey($list);
        
        $this->assign('list', $companyList);
        $this->display();
        
        
        
        
    }
    
    
      public function add() {
        if (IS_POST) {
            $data = I('post.');
            $has = D('ApiCompany')->where(array('company_name' => $data['company_name']))->count();
            if ($has) {
                $this->ajaxError('此公司已经存在，请重设！');
            }
            /*
            $data['password'] = user_md5($data['password']);
            $data['regIp'] = get_client_ip(1);
            $data['regTime'] = time();
             * 
             */
            $res = D('ApiCompany')->add($data);
            if ($res === false) {
                $this->ajaxError('操作失败');
            } else {
                $this->ajaxSuccess('添加成功');
            }
        } else {
            $this->display();
        }
    }
     public function edit() {
         if (IS_POST) {
            $data = I('post.');
            $data['id'] = intval($data['id']);
            if(!$data['id']){
                $this->ajaxError('无效操作');
            }
            $has = D('ApiCompany')->where(array('company_name' => $data['company_name']))->where('id != '.$data['id'])->count();
            if ($has) {
                $this->ajaxError('此公司已经存在，请重设！');
            }
            /*
            $data['password'] = user_md5($data['password']);
            $data['regIp'] = get_client_ip(1);
            $data['regTime'] = time();
             * 
             */
            $res = D('ApiCompany')->save($data);
            if ($res === false) {
                $this->ajaxError('操作失败');
            } else {
                $this->ajaxSuccess('添加成功');
            }
        } else {
            $id = (int)I('get.id');
            $info = D('ApiCompany')->find($id);
            $this->assign('detail',$info);
            $this->display('add');
        }
         
         
         
         
         
     }
    
    
    public function del() {
        $id = I('post.id');
        $res = D('ApiCompany')->where(array('id' => $id))->delete();
        if ($res === false) {
            $this->ajaxError('操作失败');
        } else {
            $this->ajaxSuccess('操作成功');
        }
    }
    
    
    
    
    
}
