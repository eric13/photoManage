<?php
namespace app\admin\controller;
use app\admin\controller\Base;
/**
 * 登录控制器
 * 
 * @since   2017-10-20
 * @author  songdemei <songdemei@suxuantech.com>
 */
class Login extends Base {

    public function index() {
        return $this->fetch();
    }

    public function login() {
        $pass = user_md5(input('post.password'));
        $user = input('post.username');
        $userInfo = db('ApiUser')->where(array('username' => $user, 'password' => $pass))->find();
        if (!empty($userInfo)) {
            if ($userInfo['status']) {

                //保存用户信息和登录凭证
                S($userInfo['id'], session_id(), C('ONLINE_TIME'));
                session('uid', $userInfo['id']);

                //更新用户数据
                $userData = db('ApiUserData')->where(array('uid' => $userInfo['id']))->find();
                $data = array();
                if ($userData) {
                    $data['loginTimes'] = $userData['loginTimes'] + 1;
                    $data['lastLoginIp'] = get_client_ip(1);
                    $data['lastLoginTime'] = NOW_TIME;
                    db('ApiUserData')->where(array('uid' => $userInfo['id']))->update($data);
                } else {
                    $data['loginTimes'] = 1;
                    $data['uid'] = $userInfo['id'];
                    $data['lastLoginIp'] = get_client_ip(1);
                    $data['lastLoginTime'] = NOW_TIME;
                    db('ApiUserData')->insert($data);
                }
                $this->ajaxSuccess('登录成功');
            } else {
                $this->ajaxError('用户已被封禁，请联系管理员');
            }
        } else {
            $this->ajaxError('用户名密码不正确');
        }
    }

    public function logOut() {
        S(session('uid'), null);
        session('[destroy]');
        $this->success('退出成功', url('Login/index'));
    }

    public function changeUser() {
        if (IS_POST) {
            $data = input('post.');
            $newData = array();
            if (!empty($data['nickname'])) {
                $newData['nickname'] = $data['nickname'];
            }
            if (!empty($data['password'])) {
                $newData['password'] = user_md5($data['password']);
                $newData['updateTime'] = time();
            }
            $res = db('ApiUser')->where(array('id' => session('uid')))->update($newData);
            if ($res === false) {
                $this->ajaxError('修改失败');
            } else {
                $this->ajaxSuccess('修改成功');
            }
        } else {
            $userInfo = db('ApiUser')->where(array('id' => session('uid')))->find();
            $this->assign('uname', $userInfo['username']);
            return $this->fetch('add');
        }
    }

}