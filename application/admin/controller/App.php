<?php
/**
 *
 * @since   2017/03/06 创建
 * @author  zhaoxiang <zhaoxiang051405@gmail.com>
 */

namespace app\admin\controller;

use think\Model;
use app\admin\ORG\Str;

class App extends Base {

    public function index(){
        $list = D('ApiApp')->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function edit(){
        if( IS_GET ){
            $id = (int)input('id');
            if( $id ){
                $detail = D('ApiApp')->where(array('id' => $id))->find();
                $this->assign('detail', $detail);
                return $this->fetch('add');
            }else{
                $this->redirect('add');
            }
        }elseif( IS_POST ){
            $data = I('post.');
            $res = D('ApiApp')->where(array('id' => $data['id']))->update($data);
            if( $res === false ){
                $this->ajaxError('操作失败');
            }else{
                $this->ajaxSuccess('添加成功');
            }
        }
    }

    public function add(){
        if( IS_POST ){
            $data = I('post.');
            $res = D('ApiApp')->insert($data);
            if( $res === false ){
                $this->ajaxError('操作失败');
            }else{
                $this->ajaxSuccess('添加成功');
            }
        }else{
            $data['app_id'] = Str::randString(8, 1);
            $data['app_secret'] = Str::randString(32);
            $this->assign('detail', $data);
            return $this->fetch();
        }
    }

    public function open(){
        if( IS_POST ){
            $id = I('post.id');
            if( $id ){
                Model('ApiApp')->open(array('id' => $id));
                $this->ajaxSuccess('操作成功');
            }else{
                $this->ajaxError('缺少参数');
            }
        }
    }

    public function close(){
        if( IS_POST ){
            $id = I('post.id');
            if( $id ){
                Model('ApiApp')->close(array('id' => $id));
                $this->ajaxSuccess('操作成功');
            }else{
                $this->ajaxError('缺少参数');
            }
        }
    }

    
    public function del(){
        if( IS_POST ){
            $id = I('post.id');
            if( $id ){
                Model('ApiApp')->del(array('id' => $id));
                $this->ajaxSuccess('操作成功');
            }else{
                $this->ajaxError('缺少参数');
            }
        }
    }
}