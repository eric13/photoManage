<?php
namespace app\admin\controller;

use app\admin\ORG\Auth;
use app\admin\controller\Base;

class Index extends Base{
    public function index() {
        $isAdmin = isAdministrator();
        $list = array();
        $menuAll = $this->allMenu;
        foreach ($menuAll as $key => $menu) {
            if($menu['hide'] != 0){
                unset($menuAll[$key]);
            }
        }
        
        foreach ($menuAll as $menu) {
            if($isAdmin){
                $menu['url'] = U($menu['url']);
                $list[] = $menu;
            }else{
                $authObj = new Auth();
                $authList = $authObj->getAuthList($this->uid);  
                if (in_array(strtolower($menu['url']), $authList) || in_array($menu['id'], $authList)) {
                    $menu['url'] = url($menu['url']);
                    $list[] = $menu;
                }                
            }
        }
        $this->assign('list', $list);
        return $this->fetch();
    }
    public function welcome() {
        return $this->fetch();
    }
}