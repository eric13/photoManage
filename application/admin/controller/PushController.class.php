<?php
/**
 * xlt,2017.5.15
 * 后台推送管理控制器
 */

namespace Admin\Controller;

class PushController extends BaseController {

		
    //推送信息列表
    public function index() {
	        $push = D('ApiPushrecords');
	        $condition = '';
	        if($_POST){
	        	$type = intval($_POST['type']);
	        	$keyword = addslashes((trim($_POST['keyword'])));
	        	$date = $_POST['mydate'];
	        	$status = $_POST['status'];
	        	$c_id = $_POST['c_id'];
	        	if($type == 1){
	        		$condition = "c_id='$c_id'";
	        	}
	        	elseif($type == 2 && $keyword != ''){
	        		$condition = "content like '%$keyword%'";
	        	}	 
	        	elseif($type == 3 && $keyword != ''){
	        		$condition = "cid like '%$keyword%'";
	        	}	
	        	elseif($type == 4){
	        		$condition = "FROM_UNIXTIME(send_time,'%Y-%m-%d')='$date'";
	        	}
	        	elseif($type == 5){
	        		$condition = "status=$status";
	        	}        		        	        	       	
	        }
	        $list = $push->where($condition)->order('id desc')->limit(200)->select();
	        $company = D('ApiCompany');
	        $clist = $company->select();
	        $this->assign('list', $list);
	        $this->assign('clist', $clist);
	        $this->display();
    }
    
    //推送队列列表
    public function queuelist() {
	        $push = D('ApiPushqueue');
	        $condition = '';
	        if($_POST){
	        	$type = intval($_POST['type']);
	        	$keyword = addslashes((trim($_POST['keyword'])));
	        	$date = $_POST['mydate'];
	        	$status = $_POST['status'];
	        	$c_id = $_POST['c_id'];
	        	if($type == 1){
	        		$condition = "c_id='$c_id'";
	        	}
	        	elseif($type == 2 && $keyword != ''){
	        		$condition = "content like '%$keyword%'";
	        	}	 
	        	elseif($type == 3 && $keyword != ''){
	        		$condition = "cid like '%$keyword%'";
	        	}	
	        	elseif($type == 4){
	        		$condition = "FROM_UNIXTIME(receive_time,'%Y-%m-%d')='$date'";
	        	}
	        	elseif($type == 5){
	        		$condition = "status=$status";
	        	}        		        	        	       	
	        }
	        $list = $push->where($condition)->order('id desc')->limit(200)->select();
	        $company = D('ApiCompany');
	        $clist = $company->select();
	        $this->assign('list', $list);    
	        $this->assign('clist', $clist);
	        $this->display();
    }  
      
    //推送信息配置
    public function setitem() {
        $list = D('ApiItem')->field('api_item.*,api_app.app_name')->join('api_app ON api_item.app_id = api_app.app_id')->where('app_type=2')->select();
        $item=array();
        foreach($list as $key => $value){
            $item[$key]=unserialize($value['value']);
            $item[$key]['id']=$value['id'];
            $item[$key]['key']=$value['key'];
            $item[$key]['app_name']=$value['app_name'];
        }
        $this->assign('data',$item);
        $this->display();
    }

    //编辑推送配置信息
    public function edit(){
        if( IS_GET ){
            $id = I('get.id');
            if( $id ){
                $detail = D('ApiItem')->where(array('id' => $id))->find();
                $item=unserialize($detail['value']);
                $item['id']=$detail['id'];
                $item['key']='getui';
                $data=D('ApiApp')->where(array('app_id'=>$detail['app_id']))->find();
                $item['app_id']=$data['id'];
                $list=D('ApiApp')->where('app_type=2')->select();
                $this->assign('list',$list);
                $this->assign('detail',$item);
                $this->display('add');
            }else{
                $this->redirect('add');
            }
        }elseif( IS_POST ){
            $detail = I('post.');
            //print_r($detail);die;
            $data = array();
            $mydata = array();
            $data['appid'] = addslashes((trim($_POST['appid'])));
            $data['appkey'] = addslashes((trim($_POST['appkey'])));
            $data['mastersecret'] = addslashes((trim($_POST['mastersecret'])));
            $data['host'] = addslashes((trim($_POST['host'])));
            $data['sendtimes'] = ($_POST['sendtimes'] > 1)?intval($_POST['sendtimes']):1;
            $app_id=D('ApiApp')->where(array('id'=>$detail['app_id']))->find();
            $mydata = array('key'=>'getui','value'=>serialize($data),'app_id'=>$app_id['app_id']);
            $res = D('ApiItem')->where(array('id' => $detail['id']))->save($mydata);
            if( $res === false ){
                $this->ajaxError('操作失败');
            }else{
                $this->ajaxSuccess('添加成功');
            }
        }
    }

    //新增推送配置信息
    public function add(){
        if( IS_POST ){
            $detail = I('post.');
            $data = array();
            $mydata = array();
            $data['appid'] = addslashes((trim($_POST['appid'])));
            $data['appkey'] = addslashes((trim($_POST['appkey'])));
            $data['mastersecret'] = addslashes((trim($_POST['mastersecret'])));
            $data['host'] = addslashes((trim($_POST['host'])));
            $data['sendtimes'] = ($_POST['sendtimes'] > 1)?intval($_POST['sendtimes']):1;
            $app_id=D('ApiApp')->field('app_id')->where(array('id'=>$detail['app_id']))->find();
            $mydata = array('key'=>'getui','value'=>serialize($data),'app_id'=>$app_id['app_id']);
            $res = D('ApiItem')->add($mydata);
            if( $res === false ){
                $this->ajaxError('操作失败');
            }else{
                $this->ajaxSuccess('添加成功');
            }
        }else{
            $list=D('ApiApp')->where('app_type=2')->select();
            $this->assign('list',$list);
            $this->display();
        }
    }

    //删除推送配置信息
    public function itemDel(){
        $id = I('post.id');
        $res = D('ApiItem')->where(array('id' => $id))->delete();
        if ($res === false) {
            $this->ajaxError('操作失败');
        } else {
            $this->ajaxSuccess('操作成功');
        }
    }

    //删除推送信息
    public function del() {
    	$id = I('get.id');
        $push = D('ApiPushrecords');
        $res = $push->where("id=$id")->delete();
        if($res) {
            exit('删除成功');
        }
        else{
            exit('删除失败');
        }
    }
    
    //手动发送记录表信息
    public function handpush() {
    	$id = I('get.id');
    	if($id > 0){
	        $records = D('ApiPushrecords');
	        $myrecord = $records->where("id=$id")->find();
		    $item = D('ApiItem');
	        $myitem = $item->where("`key`='getui'")->find();
	        $arr =  unserialize($myitem['value']);
	        if($myrecord['usertype'] == 1){//推送给单个用户
	             $res = pushMessageToSingle($arr['appid'],$arr['appkey'],$arr['host'],$arr['mastersecret'],$myrecord['cid'],$myrecord['content']); 
	        }else{//推送给多个用户
	    		 $cidlist = explode(',',$myrecord['cid']);
			     $res = pushMessageToList($arr['appid'],$arr['appkey'],$arr['host'],$arr['mastersecret'],$cidlist,$myrecord['content']);    	
	        }
	        if($res['result'] == 'ok'){
	        	$records->where("id=$id")->data(array('status'=>1,'adminid'=>session('uid')))->save();//修改发送状态
	            exit('发送成功');
	        }else{
	        	exit('发送失败');
	        }
    	}
    }
    //手动发送队列表信息
    public function qhandpush() {
    	$id = I('get.id');
    	if($id > 0){
	        $queue = D('ApiPushqueue');
	        $records = D('ApiPushrecords');
	        $myqueue = $queue->where("id=$id")->find();
		    $item = D('ApiItem');
	        $myitem = $item->where("`key`='getui'")->find();
	        $arr =  unserialize($myitem['value']);
	        if($myqueue['usertype'] == 1){//推送给单个用户
	             $res = pushMessageToSingle($arr['appid'],$arr['appkey'],$arr['host'],$arr['mastersecret'],$myqueue['cid'],$myqueue['content']);
	        }else{//推送给多个用户
	    		 $cidlist = explode(',',$myqueue['cid']);
			     $res = pushMessageToList($arr['appid'],$arr['appkey'],$arr['host'],$arr['mastersecret'],$cidlist,$myqueue['content']);    	
	        }
	        if($res['result'] == 'ok'){//手动发送成功
	    		$records->add(array('c_id'=>$myqueue['c_id'],'content'=>$myqueue['content'],'device_type'=>$myqueue['device_type'],'cid'=>$myqueue['cid'],'receive_time'=>$myqueue['receive_time'],
	    		'send_time'=>time(),'type'=>$myqueue['type'],'usertype'=>$myqueue['usertype'],'adminid'=>session('uid'),'status'=>1));
	    		$queue->where("id=$id")->delete();//删除本条请求信息
	            exit('发送成功');
	        }else{
	        	exit('发送失败');
	        }
    	}
    }    			   
}