<?php
/**
 *  API接口慕容复
 * @since   2017/10/27 创建
 * @author  songdemei <songdemei@suxuantech.cn>
 */

namespace app\admin\controller;
use think\Model;

class ApiManage extends Base {
    public function index() {
    	//添加排序 //add by wkj 2017-03-18
        $list = D('api_list')
                ->join("api_app"," api_app.id = api_list.app_id","left")
                ->field("api_list.*,api_app.app_name")
                ->order('api_list.id asc')
                ->select()
                ;
        //echo $list->buildSql();
        //exit();
        $this->assign('list', $list);
        //exit($this->display());
        return $this->fetch();
    }

    public function edit() {
        if( IS_GET ) {
            $id = input('id');
            $appList = D("ApiApp")->select();
            if( $id ){
                $detail = D('ApiList')->where(array('id' => $id))->find();
                $this->assign('appList',$appList);
                $this->assign('detail', $detail);
                $this->display('add');
                return $this->fetch('add');
            }else{
                $this->redirect('add');
            }
        }elseif( IS_POST ) {
            $data = I('post.');
            $res = D('ApiList')->where(array('id' => $data['id']))->update($data);
            if( $res === false ) {
                $this->ajaxError('操作失败');
            } else {
                $this->ajaxSuccess('添加成功');
            }
        }
    }

    public function add() {
        if( IS_POST ) {
            $data = I('post.');
            $res = D('ApiList')->insert($data);
            if( $res === false ) {
                $this->ajaxError('操作失败');
            } else {
                $this->ajaxSuccess('添加成功');
            }
        } else {
            $appList = D("ApiApp")->select();
            
            $this->assign('appList',$appList);
            $data['hash'] = uniqid();
            $this->assign('detail', $data);
            //$this->display();
            return $this->fetch();
        }
    }

    public function open() {
        if( IS_POST ) {
            $id = I('post.id');
            if( $id ) {
                Model('ApiList')->open(array('id' => $id));
                $this->ajaxSuccess('操作成功');
            } else {
                $this->ajaxError('缺少参数');
            }
        }
    }

    public function close() {
        if( IS_POST ) {
            $id = I('post.id');
            if( $id ) {
                Model('ApiList')->close(array('id' => $id));
                $this->ajaxSuccess('操作成功');
            } else {
                $this->ajaxError('缺少参数');
            }
        }
    }
    /**
     * 删除API记录，但字段未删除，后续优化。
     * 
     * 
     */
    public function del() {
        if( IS_POST ) {
            $id = I('post.id');
            if( $id ) {
                Model('ApiList')->del(array('id' => $id));
                $this->ajaxSuccess('操作成功');
            } else {
                $this->ajaxError('缺少参数');
            }
        }
    }
}