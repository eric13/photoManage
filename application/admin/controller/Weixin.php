<?php

namespace Admin\Controller;
use Home\Api\V10\Weixin;
/**
 * 短信管理控制器
 * @since   2017-05-19
 * @author  wangyining
 */
class WeixinController extends BaseController{

    public function company(){
        $list = D('ApiItem')->field('api_item.*,api_company.company_name')->join('api_company ON api_item.c_id = api_company.id')->where("api_item.key='weixin'")->select();
        $item=array();
        foreach($list as $key => $value){
            $item[$key]=unserialize($value['value']);
            $item[$key]['id']=$value['id'];
            $item[$key]['key']=$value['key'];
            $item[$key]['company_name']=$value['company_name'];
        }
        $this->assign('data',$item);
        $this->display();
    }

    //新增微信公众号配置信息
    public function add(){
        if( IS_POST ){
            $detail = I('post.');
            $data = array();
            $mydata = array();
            $data['appid'] = addslashes((trim($_POST['appid'])));
            $data['appsecret'] = addslashes((trim($_POST['appsecret'])));
            $data['url'] = addslashes((trim($_POST['url'])));
            $data['token'] = addslashes((trim($_POST['token'])));
            $data['encodingaeskey'] = addslashes((trim($_POST['encodingaeskey'])));
            $mydata = array('key'=>'weixin','value'=>serialize($data),'app_id'=>0,'c_id'=>$detail['c_id']);
            $res = D('ApiItem')->add($mydata);
            if( $res === false ){
                $this->ajaxError('操作失败');
            }else{
                $this->ajaxSuccess('添加成功');
            }
        }else{
            $list=D('ApiCompany')->select();
            $this->assign('list',$list);
            $this->display();
        }
    }

    //编辑微信公众号配置信息
    public function edit(){
        if( IS_GET ){
            $id = I('get.id');
            if( $id ){
                $detail = D('ApiItem')->where(array('id' => $id))->find();
                $item=unserialize($detail['value']);
                $item['id']=$detail['id'];
                $item['key']='weixin';
                $item['c_id']=$detail['c_id'];
                $list=D('ApiCompany')->select();
                $this->assign('list',$list);
                $this->assign('detail',$item);
                $this->display('add');
            }else{
                $this->redirect('add');
            }
        }elseif( IS_POST ){
            $detail = I('post.');
            //print_r($detail);die;
            $data = array();
            $mydata = array();
            $data['appid'] = addslashes((trim($_POST['appid'])));
            $data['appsecret'] = addslashes((trim($_POST['appsecret'])));
            $data['url'] = addslashes((trim($_POST['url'])));
            $data['token'] = addslashes((trim($_POST['token'])));
            $data['encodingaeskey'] = addslashes((trim($_POST['encodingaeskey'])));
            $mydata = array('key'=>'weixin','value'=>serialize($data),'app_id'=>0,'c_id'=>$detail['c_id']);
            $res = D('ApiItem')->where(array('id' => $detail['id']))->save($mydata);
            if( $res === false ){
                $this->ajaxError('操作失败');
            }else{
                $this->ajaxSuccess('添加成功');
            }
        }
    }

    //删除微信公众号配置信息
    public function del(){
        $id = I('post.id');
        $res = D('ApiItem')->where(array('id' => $id))->delete();
        if ($res === false) {
            $this->ajaxError('操作失败');
        } else {
            $this->ajaxSuccess('操作成功');
        }
    }



}