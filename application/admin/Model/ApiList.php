<?php
/**
 *
 * @since   2017-10-20 创建
 * @author  songdemei <songdemei@suxuantech.com>
 */

namespace app\admin\Model;


class ApiList extends Base {
    public function open( $where ){
        return $this->where( $where )->update( array('status' => 1) );
    }

    public function close( $where ){
        return $this->where( $where )->update( array('status' => 0) );
    }

    public function del( $where ){
        return $this->where( $where )->delete();
    }
}