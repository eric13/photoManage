<?php
/**
 *
 * @since   2017/03/07 创建
 * @author  zhaoxiang <zhaoxiang051405@gmail.com>
 */

namespace app\admin\Model;


use think\Model;

class Base extends Model {
    Protected $autoCheckFields = false;
}