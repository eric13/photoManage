<?php
/**
 *
 * @since   2017/03/07 创建
 * @author  zhaoxiang <zhaoxiang051405@gmail.com>
 */

namespace app\admin\Model;


class ApiApp extends Base {
    public function open( $where ){
        return $this->where( $where )->update( array('app_status' => 1) );
    }

    public function close( $where ){
        return $this->where( $where )->update( array('app_status' => 0) );
    }

    public function del( $where ){
        return $this->where( $where )->delete();
    }
}