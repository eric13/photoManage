<?php
/**
 * 权限和URL和Menu的关联表
 * Author: 赵翔 <756958008@qq.com>
 * Date: 16/1/16
 */
namespace Admin\Model;
use Home\ORG\Str;

class ApiProductModel extends BaseModel{

    
    
    
    function newAppCode($appCode=""){
        
        $exist = 1;
        while ($exist){
            $appCodeExist = $appCode?$appCode:Str::randString(10, 1);
            $exist = $this->where(array('app_code'=>$appCodeExist))->count();
            $appCode = "";
        }
        return $appCodeExist;
    }
}